package com.cwm.common.exception;

import com.cwm.common.Enums.ResultEnum;
import lombok.*;

import java.util.Collections;
import java.util.List;

/**
 * 异常
 *
 * @author Bill
 * @version 1.0
 * @since 2021-11-16
 */
@AllArgsConstructor
@RequiredArgsConstructor
@Getter
public abstract class BaseException extends RuntimeException {
    private final ResultEnum code;
    private List<String> parameters = Collections.emptyList();
}
