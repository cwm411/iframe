package com.cwm.common.exception;

import com.cwm.common.Enums.ResultEnum;
import lombok.EqualsAndHashCode;

import java.util.List;

/**
 * 业务异常
 *
 * @author Bill
 * @version 1.0
 * @since 2021-11-16
 */
@EqualsAndHashCode(callSuper = false)
public class BizException extends BaseException {
    public BizException(ResultEnum code) {
        super(code);
    }

    public BizException(ResultEnum code, List<String> parameters) {
        super(code, parameters);
    }
}
