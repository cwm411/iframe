package com.cwm.common.exception;

import com.cwm.common.Enums.ResultEnum;
import lombok.EqualsAndHashCode;

import java.util.List;

/**
 * 权限异常
 *
 * @author Bill
 * @version 1.0
 * @since 2021-11-16
 */
@EqualsAndHashCode(callSuper = false)
public class AuthorizationException extends BaseException {
    public AuthorizationException(ResultEnum code) {
        super(code);
    }

    public AuthorizationException(ResultEnum code, List<String> parameters) {
        super(code, parameters);
    }
}
