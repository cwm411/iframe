package com.cwm.common.utils;

import lombok.extern.slf4j.Slf4j;

/**
 * Message Util
 *
 * @author Bill
 * @version 1.0
 * @since 2021-11-15
 */
@Slf4j
public class MessageUtil {

    private static final String MESSAGE_EL_PATTERN = "([{][{])(\\w+)([}][}])";

    public static String getMessage(String messageTemplate, String... parameters) {
        String src = String.valueOf(messageTemplate);
        for (String parameter : parameters) {
            src = src.replaceFirst(MESSAGE_EL_PATTERN, parameter);
        }
        return src;
    }

}
