package com.cwm.common.utils;

import com.google.common.base.Charsets;
import com.google.common.hash.Hashing;

import java.util.regex.Pattern;

/**
 * Hash Util
 *
 * @author Bill
 * @version 1.0
 * @since 2021-11-14
 */
public class HashUtil {

    /**
     * 检查输入的字符串是否是MD5哈希
     *
     * @param str
     * @return 是否是MD5
     */
    public static boolean checkMd5(String str) {
        return Pattern.compile("^[a-f0-9]{32}$").matcher(str.toLowerCase()).matches();
    }

    /**
     * md5 hash
     *
     * @param str input string
     * @return md5 hash like: `C4C7E07F62335AE37A31E977E991A09F`
     */
    public static String md5(String str) {
        return Hashing.md5().newHasher().putString(str, Charsets.UTF_8).hash().toString().toUpperCase();
    }
}
