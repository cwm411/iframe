package com.cwm.common.message;

import lombok.Builder;
import lombok.Data;

import java.io.Serializable;

/**
 * Mail Message
 *
 * @author Bill
 * @version 1.0
 * @since 2021-11-26
 */
@Builder
@Data
public class MailMessage implements Serializable {
    private String messageID;
}
