package com.cwm.common.message;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * Mail Property
 *
 * @author Bill
 * @version 1.0
 * @since 2021-11-26
 */
@Data
@ConfigurationProperties(prefix = "mail")
@Component
public class MailProperty {
    private List<String> recipients;
}
