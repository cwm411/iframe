package com.cwm.common.message;

import lombok.Data;

/**
 * MNS Property
 *
 * @author Bill
 * @version 1.0
 * @since 2021-11-26
 */
@Data
public class MNSProperty {
    // endpoint sample: http://1360580369523386.mns.cn-shenzhen.aliyuncs.com
    // or: http://1360580369523386.mns.cn-shenzhen-internal.aliyuncs.com
    private String accountendpoint;
}
