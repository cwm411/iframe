package com.cwm.common.message;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * Aliyun Property
 *
 * @author Bill
 * @version 1.0
 * @since 2021-11-25
 */
@Data
@ConfigurationProperties(prefix = "aliyun")
@Component
public class AliyunProperty {
    private String accessKeyId;
    private String accessKeySecret;
//    private OSSProperty oss;
    private MNSProperty mns;
//    private OTSProperty ots;
}
