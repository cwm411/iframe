package com.cwm.common.message;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * Messaging Topic Property
 *
 * @author Bill
 * @version 1.0
 * @since 2021-11-26
 */
@Data
@ConfigurationProperties(prefix = "messaging.topic")
@Component
public class MessagingTopicProperty {
    private String prefix;
    private String mail;
}
