package com.cwm.common.VO;

import com.cwm.common.Enums.ResultEnum;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.google.common.base.Throwables;
import org.apache.commons.lang3.StringUtils;

/**
 * Result Wrapper
 *
 * @author Bill
 * @version 1.0
 * @since 2021-11-13
 */
public class ResultWrapper<T> {
    /**
     * 状态码
     * @mock 200
     */
    private Integer code;
    /**
     * 消息
     * @mock 成功
     */
    private String message;

    /**
     * 调试消息
     */
    private String debugMessage;
    /**
     * 结果数据
     */
    private T data;

    public static <T> ResultWrapper<T> ok() {
        return ok(null);
    }

    public static <T> ResultWrapper<T> ok(T data) {
        return of(ResultEnum.SUCCESS, "成功", null, data);
    }

    public static <T> ResultWrapper<T> error(ResultEnum code, String message, Throwable e) {
        return error(code, message, Throwables.getStackTraceAsString(e));
    }

    public static <T> ResultWrapper<T> error(ResultEnum code, String message, String debugMessage) {
        return of(code, message, debugMessage, null);
    }

    public static <T> ResultWrapper<T> of(ResultEnum code, String message, String debugMessage, T data) {
        ResultWrapper<T> resultWrapper = new ResultWrapper<>();
        resultWrapper.setCode(code.getCode());
        resultWrapper.setMessage(message);
        resultWrapper.setDebugMessage(StringUtils.trimToNull(debugMessage));
        resultWrapper.setData(data);
        return resultWrapper;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    @JsonInclude(JsonInclude.Include.NON_NULL)
    public String getDebugMessage() {
        return debugMessage;
    }

    public void setDebugMessage(String debugMessage) {
        this.debugMessage = debugMessage;
    }

    @JsonInclude(JsonInclude.Include.NON_NULL)
    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }


}
