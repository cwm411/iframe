package com.cwm.frame4.controller;


import com.cwm.common.VO.ResultVO;
import com.cwm.common.VO.ResultWrapper;
import com.cwm.frame4.entity.DemoEntity;
import com.cwm.frame4.service.DemoService;
import lombok.extern.slf4j.Slf4j;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @author: zzx
 * @date: 2018/10/25 16:48
 * @description: demo
 */
@RestController
@RequestMapping("/test")
@Slf4j
public class DemoController {
    @Autowired
    private DemoService orderService;

    @RequestMapping("/getUser")
    public ResultWrapper getUser(){
        List<DemoEntity> result = orderService.getUser();
        log.info("这是log4j2的日志测试，info级别");
        log.warn("这是log4j2的日志测试，warn级别");
        log.error("这是log4j2的日志测试，error级别");

        //如果在日志输出中想携带参数的化，可以这样设置
        String bug = "约翰·冯·诺依曼 保佑，永无BUG";
        log.info("这是带参数的输出格式:{}",bug);
        return ResultWrapper.ok(result);
    }

}