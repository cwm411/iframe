package com.cwm.frame4.dao;

import com.cwm.frame4.entity.DemoEntity;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * @author: zzx
 * @date: 2018/10/25 17:05
 * @description:
 */
@Mapper
public interface DemoMapper {

    List<DemoEntity> getUser();

}
