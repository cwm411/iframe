package com.cwm.frame4.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.cwm.frame4.entity.UserInfo;
import org.apache.ibatis.annotations.Mapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author cwm
 * @since 2022-01-13
 */
@Mapper
public interface UserInfoMapper extends BaseMapper<UserInfo> {

}
