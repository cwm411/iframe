package com.cwm.frame4.dao;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.cwm.frame4.entity.*;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Component;

/**
 * @author: zzx
 * @date: 2018/10/18 14:59
 * @description: 用户dao层
 */
@Mapper
public interface UserMapper  extends BaseMapper<User> {

    //通过username查询用户
    SelfUserDetails getUser(@Param("username") String username);

}