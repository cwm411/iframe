package com.cwm.frame4.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.cwm.frame4.entity.User;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author cwm
 * @since 2022-01-13
 */
public interface IUserService extends IService<User> {

}
