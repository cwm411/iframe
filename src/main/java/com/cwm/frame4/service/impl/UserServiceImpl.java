package com.cwm.frame4.service.impl;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.cwm.frame4.dao.UserMapper;
import com.cwm.frame4.entity.User;
import com.cwm.frame4.service.IUserService;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author cwm
 * @since 2022-01-13
 */
@Service
public class UserServiceImpl extends ServiceImpl<UserMapper, User> implements IUserService {

}
