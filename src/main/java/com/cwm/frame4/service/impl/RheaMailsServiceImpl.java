package com.cwm.frame4.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.cwm.frame4.dao.RheaMailsMapper;
import com.cwm.frame4.entity.RheaMails;
import com.cwm.frame4.service.IRheaMailsService;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author cwm
 * @since 2022-01-13
 */
@Service
public class RheaMailsServiceImpl extends ServiceImpl<RheaMailsMapper, RheaMails> implements IRheaMailsService {

}
