package com.cwm.frame4.service.impl;

import com.cwm.frame4.dao.DemoMapper;
import com.cwm.frame4.entity.DemoEntity;
import com.cwm.frame4.service.DemoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author: zzx
 * @date: 2018/10/25 17:03
 * @description:
 */
@Service
public class DemoServiceImpl implements DemoService {

    @Autowired
    private DemoMapper demoMapper;

    @Override
    public List<DemoEntity> getUser() {
        return demoMapper.getUser();
    }
}
