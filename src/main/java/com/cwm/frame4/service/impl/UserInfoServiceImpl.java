package com.cwm.frame4.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.cwm.frame4.dao.UserInfoMapper;
import com.cwm.frame4.entity.UserInfo;
import com.cwm.frame4.service.IUserInfoService;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author cwm
 * @since 2022-01-13
 */
@Service
public class UserInfoServiceImpl extends ServiceImpl<UserInfoMapper, UserInfo> implements IUserInfoService {

}
