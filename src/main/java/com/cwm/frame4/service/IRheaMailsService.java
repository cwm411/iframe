package com.cwm.frame4.service;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.IService;
import com.cwm.frame4.entity.RheaMails;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author cwm
 * @since 2022-01-13
 */
public interface IRheaMailsService extends IService<RheaMails> {

}
