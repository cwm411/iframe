package com.cwm.frame4.service;

/**
 * Mail Service
 *
 * @author Bill
 * @version 1.0
 * @since 2021-11-26
 */
public interface MailService {

    void notifyOperator(Throwable e);

}
