package com.cwm.frame4.service;


import com.cwm.common.message.MailMessage;

import java.io.Closeable;

/**
 * Message Service
 *
 * @author Bill
 * @version 1.0
 * @since 2021-11-26
 */
public interface MessageService extends Closeable {

    void pubTopicMail(MailMessage mailMessage);

}
