package com.cwm.frame4.entity;

import lombok.Data;

/**
 * @author: zzx
 * @date: 2018/10/25 17:01
 * @description:
 */
@Data
public class DemoEntity {

    private Integer id;

    private Integer age;

    private String name;

    private Float height;

}
