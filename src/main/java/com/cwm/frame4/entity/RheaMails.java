package com.cwm.frame4.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.*;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * <p>
 * 
 * </p>
 *
 * @author cwm
 * @since 2022-01-13
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@Builder
public class RheaMails implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    private String messageId;

    private String subject;

    private String content;

    private String contentMd5;

    private String recipients;

    private Integer status;

    private String errorStack;

    private Boolean enabled;

    private Boolean deleted;

    private LocalDateTime createdAt;

    private LocalDateTime updatedAt;


}
